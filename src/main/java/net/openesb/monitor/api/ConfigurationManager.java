package net.openesb.monitor.api;

import java.util.Set;
import net.openesb.monitor.api.model.Instance;

/**
 *
 * @author david
 */
public interface ConfigurationManager {
    
    Set<Instance> getAllInstances();
    
    Instance getInstance(String name);
}
