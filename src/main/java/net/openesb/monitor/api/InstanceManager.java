/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.monitor.api;

import java.util.Set;

/**
 *
 * @author david
 */
public interface InstanceManager {
    
    Set<String> getComponents();
    
    Set<String> getServiceAssemblies();
    
    Set<String> getEndpointByComponent(String componentId);
}
