/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.monitor.api;

import net.openesb.monitor.api.model.ComponentStatistics;
import net.openesb.monitor.api.model.EndpointStatistics;

/**
 *
 * @author david
 */
public interface InstanceStatisticsManager {
    
    ComponentStatistics getComponentStatistics(String componentId);
    
    EndpointStatistics getEndpointStatistics(String endpointName);
}
