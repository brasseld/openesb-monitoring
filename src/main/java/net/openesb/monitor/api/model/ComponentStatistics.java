/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.monitor.api.model;

/**
 *
 * @author david
 */
public class ComponentStatistics extends Statistics {

    private long activeExchanges;
    private long maxActiveExchanges;
    private long queuedExchanges;
    private long maxQueuedExchanges;
    private long activeEndpoints;
    private long sendRequest;
    private long receiveRequest;
    private long sendReply;
    private long receiveReply;
    private long sendFault;
    private long receiveFault;
    private long sendDONE;
    private long receiveDONE;
    private long sendERROR;
    private long receiveERROR;
    private long deadWait;
    private long timeOut;
    private long sendCount;
    private long sendSyncCount;
    private long acceptCount;
    private long responseTimeMin;
    private long responseTimeAvg;
    private long responseTimeMax;
    private long responseTimeStd;
    private long statusTimeMin;
    private long statusTimeAvg;
    private long statusTimeMax;
    private long statusTimeStd;
    private long nMRTimeMin;
    private long nMRTimeAvg;
    private long nMRTimeMax;
    private long nMRTimeStd;
    private long componentTimeMin;
    private long componentTimeAvg;
    private long componentTimeMax;
    private long componentTimeStd;
    private long channelTimeMin;
    private long channelTimeAvg;
    private long channelTimeStd;

    public long getActiveExchanges() {
        return activeExchanges;
    }

    public void setActiveExchanges(long activeExchanges) {
        this.activeExchanges = activeExchanges;
    }

    public long getMaxActiveExchanges() {
        return maxActiveExchanges;
    }

    public void setMaxActiveExchanges(long maxActiveExchanges) {
        this.maxActiveExchanges = maxActiveExchanges;
    }

    public long getQueuedExchanges() {
        return queuedExchanges;
    }

    public void setQueuedExchanges(long queuedExchanges) {
        this.queuedExchanges = queuedExchanges;
    }

    public long getMaxQueuedExchanges() {
        return maxQueuedExchanges;
    }

    public void setMaxQueuedExchanges(long maxQueuedExchanges) {
        this.maxQueuedExchanges = maxQueuedExchanges;
    }

    public long getActiveEndpoints() {
        return activeEndpoints;
    }

    public void setActiveEndpoints(long activeEndpoints) {
        this.activeEndpoints = activeEndpoints;
    }

    public long getSendRequest() {
        return sendRequest;
    }

    public void setSendRequest(long sendRequest) {
        this.sendRequest = sendRequest;
    }

    public long getReceiveRequest() {
        return receiveRequest;
    }

    public void setReceiveRequest(long receiveRequest) {
        this.receiveRequest = receiveRequest;
    }

    public long getSendReply() {
        return sendReply;
    }

    public void setSendReply(long sendReply) {
        this.sendReply = sendReply;
    }

    public long getReceiveReply() {
        return receiveReply;
    }

    public void setReceiveReply(long receiveReply) {
        this.receiveReply = receiveReply;
    }

    public long getSendFault() {
        return sendFault;
    }

    public void setSendFault(long sendFault) {
        this.sendFault = sendFault;
    }

    public long getReceiveFault() {
        return receiveFault;
    }

    public void setReceiveFault(long receiveFault) {
        this.receiveFault = receiveFault;
    }

    public long getSendDONE() {
        return sendDONE;
    }

    public void setSendDONE(long sendDONE) {
        this.sendDONE = sendDONE;
    }

    public long getReceiveDONE() {
        return receiveDONE;
    }

    public void setReceiveDONE(long receiveDONE) {
        this.receiveDONE = receiveDONE;
    }

    public long getSendERROR() {
        return sendERROR;
    }

    public void setSendERROR(long sendERROR) {
        this.sendERROR = sendERROR;
    }

    public long getReceiveERROR() {
        return receiveERROR;
    }

    public void setReceiveERROR(long receiveERROR) {
        this.receiveERROR = receiveERROR;
    }

    public long getDeadWait() {
        return deadWait;
    }

    public void setDeadWait(long deadWait) {
        this.deadWait = deadWait;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public long getSendCount() {
        return sendCount;
    }

    public void setSendCount(long sendCount) {
        this.sendCount = sendCount;
    }

    public long getSendSyncCount() {
        return sendSyncCount;
    }

    public void setSendSyncCount(long sendSyncCount) {
        this.sendSyncCount = sendSyncCount;
    }

    public long getAcceptCount() {
        return acceptCount;
    }

    public void setAcceptCount(long acceptCount) {
        this.acceptCount = acceptCount;
    }

    public long getResponseTimeMin() {
        return responseTimeMin;
    }

    public void setResponseTimeMin(long responseTimeMin) {
        this.responseTimeMin = responseTimeMin;
    }

    public long getResponseTimeAvg() {
        return responseTimeAvg;
    }

    public void setResponseTimeAvg(long responseTimeAvg) {
        this.responseTimeAvg = responseTimeAvg;
    }

    public long getResponseTimeMax() {
        return responseTimeMax;
    }

    public void setResponseTimeMax(long responseTimeMax) {
        this.responseTimeMax = responseTimeMax;
    }

    public long getResponseTimeStd() {
        return responseTimeStd;
    }

    public void setResponseTimeStd(long responseTimeStd) {
        this.responseTimeStd = responseTimeStd;
    }

    public long getStatusTimeMin() {
        return statusTimeMin;
    }

    public void setStatusTimeMin(long statusTimeMin) {
        this.statusTimeMin = statusTimeMin;
    }

    public long getStatusTimeAvg() {
        return statusTimeAvg;
    }

    public void setStatusTimeAvg(long statusTimeAvg) {
        this.statusTimeAvg = statusTimeAvg;
    }

    public long getStatusTimeMax() {
        return statusTimeMax;
    }

    public void setStatusTimeMax(long statusTimeMax) {
        this.statusTimeMax = statusTimeMax;
    }

    public long getStatusTimeStd() {
        return statusTimeStd;
    }

    public void setStatusTimeStd(long statusTimeStd) {
        this.statusTimeStd = statusTimeStd;
    }

    public long getnMRTimeMin() {
        return nMRTimeMin;
    }

    public void setnMRTimeMin(long nMRTimeMin) {
        this.nMRTimeMin = nMRTimeMin;
    }

    public long getnMRTimeAvg() {
        return nMRTimeAvg;
    }

    public void setnMRTimeAvg(long nMRTimeAvg) {
        this.nMRTimeAvg = nMRTimeAvg;
    }

    public long getnMRTimeMax() {
        return nMRTimeMax;
    }

    public void setnMRTimeMax(long nMRTimeMax) {
        this.nMRTimeMax = nMRTimeMax;
    }

    public long getnMRTimeStd() {
        return nMRTimeStd;
    }

    public void setnMRTimeStd(long nMRTimeStd) {
        this.nMRTimeStd = nMRTimeStd;
    }

    public long getComponentTimeMin() {
        return componentTimeMin;
    }

    public void setComponentTimeMin(long componentTimeMin) {
        this.componentTimeMin = componentTimeMin;
    }

    public long getComponentTimeAvg() {
        return componentTimeAvg;
    }

    public void setComponentTimeAvg(long componentTimeAvg) {
        this.componentTimeAvg = componentTimeAvg;
    }

    public long getComponentTimeMax() {
        return componentTimeMax;
    }

    public void setComponentTimeMax(long componentTimeMax) {
        this.componentTimeMax = componentTimeMax;
    }

    public long getComponentTimeStd() {
        return componentTimeStd;
    }

    public void setComponentTimeStd(long componentTimeStd) {
        this.componentTimeStd = componentTimeStd;
    }

    public long getChannelTimeMin() {
        return channelTimeMin;
    }

    public void setChannelTimeMin(long channelTimeMin) {
        this.channelTimeMin = channelTimeMin;
    }

    public long getChannelTimeAvg() {
        return channelTimeAvg;
    }

    public void setChannelTimeAvg(long channelTimeAvg) {
        this.channelTimeAvg = channelTimeAvg;
    }

    public long getChannelTimeStd() {
        return channelTimeStd;
    }

    public void setChannelTimeStd(long channelTimeStd) {
        this.channelTimeStd = channelTimeStd;
    }
}
