/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.monitor.api.model;

/**
 *
 * @author david
 */
public abstract class EndpointStatistics extends Statistics {
    
    private String endpointName;

    public String getEndpointName() {
        return endpointName;
    }

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }
}
