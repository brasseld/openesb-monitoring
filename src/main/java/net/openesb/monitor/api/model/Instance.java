/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.monitor.api.model;

import java.net.URL;

/**
 *
 * @author david
 */
public class Instance {
    
    private URL url;
    private String name;
    private String username;
    private String password;

    public URL getUrl() {
        return url;
    }

    public String getHost() {
        return url.getHost();
    }
    
    public int getPort() {
        return url.getPort();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
