/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.monitor.api.model;

import java.io.Serializable;

/**
 *
 * @author david
 */
public abstract class Statistics implements Serializable {
    
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
